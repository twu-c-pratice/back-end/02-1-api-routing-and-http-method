package com.twuc.webApp;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UserControllerTest {
    @Test
    public void should_return_message_with_user_id() {
        UserController userController = new UserController();
        String expected = userController.getBooksByUserId(2);
        assertEquals(expected, "The book for user 2");
    }
}
