package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerIntegrationTest {
    @Autowired private MockMvc mockMvc;

    @Test
    public void should_return_books_by_user_id_with_status_code() throws Exception {
        int userId = 2;
        String except = "The book for user " + String.valueOf(userId);
        mockMvc.perform(
                        MockMvcRequestBuilders.get(
                                "/api/users/" + String.valueOf(userId) + "/books"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(except));
    }

    @Test
    public void should_return_good() throws Exception {
        String expect = "good";
        mockMvc.perform(MockMvcRequestBuilders.get("/api/segments/good"))
                .andExpect(MockMvcResultMatchers.content().string("good"));
    }

    @Test
    void should_return_when_using_single_wildcard_() throws Exception {
        String expected = "great";
        mockMvc.perform(MockMvcRequestBuilders.get("/api/user/"))
                .andExpect(MockMvcResultMatchers.content().string(expected));
    }

    @Test
    void should_failed_when_multiple_wildcard_using_single_wildcard() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/usser/"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    void should_return_when_using_multiple_wildcard() throws Exception {
        String expected = "nice";
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcards/anything"))
                .andExpect(MockMvcResultMatchers.content().string(expected));
    }

    @Test
    void should_return_when_using_multiple_wildcard_2() throws Exception {
        String expected = "nice";
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcards/anything"))
                .andExpect(MockMvcResultMatchers.content().string(expected));
    }

    @Test
    void should_failed_when_using_multiple_wildcard_3() throws Exception {
        String excepted = "nice";
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcard/before/after"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    void should_return_when_using_multiple_wildcard_4() throws Exception {
        String expected = "hello";
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcards/anything/after/hello"))
                .andExpect(MockMvcResultMatchers.content().string(expected));
    }

    @Test
    void should_return_when_using_regex_matcher() throws Exception {
        String excepted = "Hello, World!";
        mockMvc.perform(MockMvcRequestBuilders.get("/api/regex/add"))
                .andExpect(MockMvcResultMatchers.content().string(excepted));
    }

    @Test
    void should_return_when_using_query_string() throws Exception {
        int id = 909;
        mockMvc.perform(MockMvcRequestBuilders.get(String.format("/api/usersId?id=%s", id)))
                .andExpect(
                        MockMvcResultMatchers.content().string(String.format("user id is %s", id)));
    }

    @Test
    void should_return_when_without_query_string() throws Exception {
        int id = 909;
        mockMvc.perform(MockMvcRequestBuilders.get(String.format("/api/usersId", id)))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }
}
