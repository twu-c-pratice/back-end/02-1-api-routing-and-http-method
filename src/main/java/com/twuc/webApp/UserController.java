package com.twuc.webApp;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class UserController {
    @GetMapping("/users/{userId}/books/")
    public String getBooksByUserId(@PathVariable int userId) {
        return String.format("The book for user %s", userId);
    }

    @GetMapping("/users/{id}/books")
    public String getBooksById(@PathVariable("id") int userId) {
        return String.format("The book for user %s", userId);
    }

    @GetMapping("/segments/good")
    public String getGood() {
        return "good";
    }

    @GetMapping("/segments/{segmentName}")
    public String getBetter(@PathVariable String segmentName) {
        return "better";
    }

    @GetMapping("/us?r")
    public String getGreat() {
        return "great";
    }

    @GetMapping("/wildcards/*")
    public String getNice() {
        return "nice";
    }

    @GetMapping("/*/anything")
    public String getNiceAgain() {
        return "nice";
    }

    @GetMapping("/wildcard/before/*/after")
    public String getNiceOneMoreTime() {
        return "nice";
    }

    @GetMapping("/wildcards/**/hello")
    public String getHello() {
        return "hello";
    }

    @GetMapping("/regex/{[abcd]+}")
    public String getHelloWorld() {
        return "Hello, World!";
    }

    @GetMapping("/usersId")
    public String getUserId(@RequestParam int id) {
        return String.format("user id is %s", id);
    }
}
